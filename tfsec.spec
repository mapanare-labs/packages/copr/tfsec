%define debug_package %{nil}

Name:           tfsec
Version:        1.28.11
Release:        1%{?dist}
Summary:        Security scanner for your Terraform code

License:        MIT
URL:            https://aquasecurity.github.io/tfsec
Source0:        https://github.com/aquasecurity/%{name}/releases/download/v%{version}/%{name}_%{version}_linux_amd64.tar.gz

%description
Security scanner for your Terraform code

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin/
install -p -m 755 %{name}-checkgen %{buildroot}/usr/bin/tfsec-checkgen

%files
%license LICENSE
%doc README.md
/usr/bin/tfsec
/usr/bin/tfsec-checkgen

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.28.5

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.28.4

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.28.1

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 1.28.0

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
